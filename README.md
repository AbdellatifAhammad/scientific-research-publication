### Sientific Research publications Project
---
##### Descrition :100: 
> this project is create to add and handle scientific publication inside a specific organization usign spring boot and also angular in frontend



##### application Screens :lollipop: 

- profs table

![](https://i.imgur.com/9AoJwVL.png)


- add new Prof

![](https://i.imgur.com/wuFzQEZ.png)


- Publications table

![](https://i.imgur.com/0rkyBzj.png)



- add new publication 

![](https://i.imgur.com/rZcPzE8.png)



- Phd students table 

![](https://i.imgur.com/qAe6rgV.png)


- adding new Phd students 

![](https://i.imgur.com/nGOhagk.png)


- Authors table

![](https://i.imgur.com/uB2P0ZS.png)


- adding new Author

![](https://i.imgur.com/uC7srOv.png)


---- 
created BY : abdellatif Ahammad
